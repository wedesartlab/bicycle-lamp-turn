#include <Arduino.h>

#include "Led.h"
#include "Button.h"
#include "LampTurn.h"
#include "LampTurnController.h"

Led leftLeds(9, LED_MODE_BLINK);
Button leftBtn(4);

Led rightLeds(8, LED_MODE_BLINK);
Button rightBtn(2);

LampTurn leftLampTurn;
LampTurn rightLampTurn;

Button btnReset(3);

LampTurnController controller;

void setup() {
  Serial.begin(9600);

  leftLampTurn.setElements(&leftBtn, &leftLeds);
  rightLampTurn.setElements(&rightBtn, &rightLeds);

  controller.addLamp(&leftLampTurn);
  controller.addLamp(&rightLampTurn);
  controller.setResetButton(&btnReset);
}

void loop() {
  controller.update();
}
