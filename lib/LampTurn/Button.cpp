#include "Arduino.h"
#include "Button.h"

Button::Button(byte btnPin) {
  _btnPin = btnPin;
  pinMode(_btnPin, INPUT_PULLUP);
}

boolean Button::isActive() {
  return LOW == digitalRead(_btnPin);
}
