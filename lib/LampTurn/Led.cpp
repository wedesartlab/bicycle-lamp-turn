#include "Led.h"
#include "Arduino.h"

Led::Led(byte ledPin) {
  init(ledPin);
}

Led::Led(byte ledPin, byte mode) {
  init(ledPin);
  _mode = mode;
}

void Led::init(byte ledPin) {
  _ledPin = ledPin;
  pinMode(_ledPin, OUTPUT);
}

void Led::on() {
  switch (_mode) {
    case LED_MODE_STATIC:
      doOn();
    break;
    case LED_MODE_BLINK:
      doOn();
      delay(150);
      off();
      delay(150);
    break;
  }
}

void Led::off() {
  digitalWrite(_ledPin, LOW);
}

void Led::doOn() {
  digitalWrite(_ledPin, HIGH);
}
