#ifndef LampTurnController_h
#define LampTurnController_h

#include "Arduino.h"
#include "Timer.h"
#include "LampTurn.h"
#include "Button.h"

class LampTurnController
{
  public:
    LampTurnController();
    void addLamp(LampTurn *lamp);
    void setResetButton(Button *btn);

    void update();
  private:
    LampTurn* _lamp[2];
    static LampTurn* _activeLamp;
    byte _lampCount = 0;
    Button* _btnReset;
    Timer _timer;
    void checkLamps();
    void checkResetButton();
    void offAllLamps();
    static LampTurn* getActiveLamp();
    static void timerAction();
};

#endif
