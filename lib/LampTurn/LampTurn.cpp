#include "LampTurn.h"

#include "Arduino.h"
#include "Led.h"
#include "Button.h"

LampTurn::LampTurn() {}

void LampTurn::setElements(Button *btn, Led *led) {
  _btn = btn;
  _led = led;
}

boolean LampTurn::isButtonActive() {
  return _btn->isActive();
}

void LampTurn::on() {
  _led->on();
}

void LampTurn::off() {
  _led->off();
}
