#ifndef LampTurn_h
#define LampTurn_h

#include "Arduino.h"
#include "Led.h"
#include "Button.h"

class LampTurn
{
  public:
    LampTurn();
    void setElements(Button *btn, Led *led);

    boolean isButtonActive();

    void on();
    void off();
  private:
    Button* _btn;
    Led* _led;
};

#endif
