#include "LampTurnController.h"

#include "Arduino.h"
#include "LampTurn.h"
#include "Button.h"

LampTurn *LampTurnController::_activeLamp;

LampTurnController::LampTurnController() {
  _timer.every(100, timerAction);
}

void LampTurnController::addLamp(LampTurn *lamp) {
  _lamp[_lampCount++] = lamp;
}

void LampTurnController::setResetButton(Button *btn) {
  _btnReset = btn;
}

void LampTurnController::update() {
  checkLamps();
  checkResetButton();

  _timer.update();
}

LampTurn* LampTurnController::getActiveLamp() {
  return _activeLamp;
}

void LampTurnController::checkLamps() {
  LampTurn* lamp;
  for (byte i = 0; i < _lampCount; i++) {
    if (_lamp[i]->isButtonActive()) {
      lamp = _lamp[i];
    }
  }

  if (NULL != lamp && _activeLamp != lamp) {
    _activeLamp = lamp;
    offAllLamps();
  }
}

void LampTurnController::checkResetButton() {
  if (_btnReset->isActive()) {
    _activeLamp = NULL;
    offAllLamps();
  }
}

void LampTurnController::offAllLamps() {
  for (byte i = 0; i < _lampCount ; i++) {
    if (_activeLamp != _lamp[i]) {
      _lamp[i]->off();
    }
  }
}

void LampTurnController::timerAction() {
  LampTurn* lamp = LampTurnController::getActiveLamp();
  if (NULL != lamp) {
    lamp->on();
  }
}
