#ifndef Button_h
#define Button_h

#include "Arduino.h"

class Button {
  public:
	  Button(byte btnPin);
    boolean isActive();
  private:
    byte _btnPin;
};

#endif
