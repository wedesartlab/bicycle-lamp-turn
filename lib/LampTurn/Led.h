#ifndef Led_h
#define Led_h

#include "Arduino.h"

const byte LED_MODE_STATIC = 0;
const byte LED_MODE_BLINK = 1;

class Led {
  public:
	  Led(byte ledPin);
    Led(byte ledPin, byte mode);
    void on();
	  void off();
  private:
    byte _ledPin;
    byte _mode = LED_MODE_STATIC;
    void init(byte ledPin);
    void doOn();
};

#endif
